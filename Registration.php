<!DOCTYPE html>
<!--
Form validation using javascript and php.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        <script type="text/javascript">
function check_info() {
var username=document.getElementById('username').value;
var name=document.getElementById('name').value;
var surname=document.getElementById('surname').value;
var postal=document.getElementById('postal').value;
var residential=document.getElementById('residential').value;
var gender=document.getElementById('gender').value;
var city=document.getElementById('city').value;
var course=document.getElementById('course').value;
var password=document.getElementById('password').value;
var password2=document.getElementById('password2').value;
var dob=document.getElementById('dob').value;
var contacts=document.getElementById('contacts').value;
//var file=document.getElementById('file').value;

if(username=="" || name=="" || surname=="" || postal=="" || residential=="" || gender=="" || city=="" || course==""  || password="" password2=="" || dob=="" || contacts=="" || //file=="") 
{alert('FILL IN ALL THE REQUIRED FIELDS');
return false;
}
else{
return true;
}
}

</script>
    </head>
    <body>
        <?php
        
        $username= $name= $surname= $postal= $residential= $gender= $city= $course= $password= $password2= $dob= $contacts= $file="";
        
       if($_SERVER["REQUEST_METHOD"]=="POST")
       {
        $username= test_input($_POST["username"]); 
        $name= test_input($_POST["name"]); 
         $surname= test_input($_POST["surname"]);        
          $postal= test_input($_POST["postal"]); 
        $residential= test_input($_POST["residential"]); 
         $gender= test_input($_POST["gender"]);           //$username= test_input($_POST["username"]); 
        $city= test_input($_POST["city"]); 
         $course= test_input($_POST["course"]);   
          $password= test_input($_POST["password"]); 
          $password2= test_input($_POST["password2"]); 
        $dob= test_input($_POST["dob"]); 
         $contacts= test_input($_POST["contacts"]);
         $file= test_input($_POST["file"]);
         
       }
       
       function test_input($data)
       {
           
        $data=trim($data);
        $data=  stripslashes($data);
        $data;
        $data=  htmlspecialchars($data);
        return $data;
       }
        ?>
 <center>
   
<table border="0" bgcolor="lightblue">
    <form method="post" action="Loggedin.php"  onsuspend=return test_input();>
 <tr>
       <th><h3>Registration Form</h3></th>
      </tr>
<tr>
<td>Username</td>
<td><INPUT TYPE="text" name="username" id="username" required/></td>
</tr>

<tr>
<td>Name</td>
<td><INPUT TYPE="text" name="name" id="name" required/></td>
</tr>

<tr>
<td>Surname</td>
<td><INPUT TYPE="text" name="surname" id="surname" required/></td>
</tr>

<tr>
<td>Postal Address</td>
<td><INPUT TYPE="text" name="postal" id="postal" required/></td>
</tr>

<tr>
<td>Residential Address</td>
<td><INPUT TYPE="text" name="residential" id="residential" required/></td>
</tr>

<tr>
<td>Gender</td>
<td>Male<INPUT TYPE="radio" name="radio" value="Male" id="male" required/>
Female<INPUT TYPE="radio" name="radio" value="Female" id="female" required/></td>
<br><br>
</tr>

<tr>
<td>City</td>
<td><select id="city">
<option value="city">select...</option>
<option value="city">Polokwane</option>
<option value="city">Cape Town</option>
<option value="city">Durban</option>
</select></td>
<br><br>
</tr>

<tr>
<td>Course</td>
<td><select id="course">
<option value="course">select course</option>
<option value="first">Diploma in IT 3 years</option>
<option value="second">Computer Applications</option>
<option value="third">PC Engineering</option>
</select></td>
<br><br>
</tr>

<tr>
<td>Password</td>
<td><INPUT TYPE="password" name="password" id="password" required/></td>
</tr>

<tr>
<td>RE-Type Password</td>
<td><INPUT TYPE="password" name="password2" id="password2" required/></td>
</tr>

<tr>
<td>DOB</td>
<td><INPUT TYPE="text" name="dob" id="dob" required></td>
</tr>

<tr>
<td>Cell phone No</td>
<td><INPUT TYPE="text" name="contacts" id="contacts" required/></td>
</tr>

<tr>
<td>Upload Picture</td>
<td><INPUT TYPE="file" name="file" id="file" required/></td>
<br><br>
</tr>

<tr>
<td><INPUT TYPE="reset" value="Reset"></td>

<td><INPUT TYPE="submit" value="Submit Form"/></td>
</tr>

</center>

</center>
</form>
</table>       
        

    </body>
</html>
